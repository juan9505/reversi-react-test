import React, { Component } from 'react';
import { Provider, connect } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import reducers from './reducers';
import Home from './components/Home';

class App extends Component {

  render() {

    const createStoreWithMiddleware = applyMiddleware(ReduxThunk)(createStore);

    return (
      <Provider store={createStoreWithMiddleware(reducers)}>
        <div>
          <Home />
        </div>
      </Provider>
    );
  }
}

export default App;
