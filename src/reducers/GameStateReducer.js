import { GAME_STATE_UPDATE, GAME_RESET } from 'actions/types';

const INITIAL_STATE = {
    "gameState": "",
    "currentPlayer": "",
    "whiteCount": 0,
    "blackCount": 0,
    "boardRows": []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GAME_STATE_UPDATE:
            return action.payload;
        case GAME_RESET:
            return { ...state, 'gameState': 'delete'}
        default:
            return state;
    }
};