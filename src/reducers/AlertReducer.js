import { ALERT_UPDATE } from 'actions/types';

const INITIAL_STATE = "";

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ALERT_UPDATE:
            return action.payload;
        default:
            return state;
    }
};