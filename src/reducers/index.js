import  { combineReducers } from 'redux';
import GameStateReducer from './GameStateReducer';
import LoaderReducer from './LoaderReducer';
import AlertReducer from './AlertReducer';


export default combineReducers ({
    gameState: GameStateReducer,
    loaderState: LoaderReducer,
    alertState: AlertReducer
});