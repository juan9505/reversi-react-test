import { LOADER_UPDATE } from 'actions/types';

const INITIAL_STATE = false;

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LOADER_UPDATE:
            return action.payload;
        default:
            return state;
    }
};