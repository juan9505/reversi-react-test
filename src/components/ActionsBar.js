import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'actions';
import Board from './Board';

class ActionsBar extends Component {

    constructor(props) {
        super(props);
        this.resetGame = this.resetGame.bind(this);
    }

    resetGame() {
        this.props.resetGame();
    }
    render() {
        const currentPlayer = () => {
            const player = this.props.gameState.currentPlayer;
            if (player === 'WHITE') {
                return 'Blanco';
            }
            else if (player === 'BLACK') {
                return 'Negro'
            }
        }

        return (
            <div className="actions-bar">
                <div className="row justify-content-center info-player">
                    <h4>Turno: {currentPlayer()}</h4>
                </div>
                <div className="row justify-content-center data-player">
                    <div className="col-6 col-md-4 col-xl-12 white d-flex justify-content-center align-items-center">
                        <div className="color-player"></div>
                        <h2 className="current-count">{this.props.gameState.whiteCount}</h2>
                    </div>
                    <div className="col-6 col-md-4 col-xl-12 black d-flex justify-content-center align-items-center">
                        <div className="color-player"></div>
                        <h2 className="current-count">{this.props.gameState.blackCount}</h2>
                    </div>
                    <div className="col-12 col-md-4 col-xl-12 d-flex justify-content-center align-items-center">
                        <div className="reset-button btn" onClick={this.resetGame}><i className="fa fa-refresh fa-lg" aria-hidden="true"></i><span>REINICIAR</span></div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return { gameState: state.gameState };
};

export default connect(mapStateToProps, actions)(ActionsBar);