import React, { Component } from 'react';
import Board from './Board';
import ActionsBar from './ActionsBar';
import Loader from './Loader';
import AlertBox from './AlertBox';

class Home extends Component {

    render() {

        return (
            <div className="main container d-flex flex-column justify-content-center align-content-center">
                <h1 className="row justify-content-center align-content-center" style={{marginBottom: '20px'}}>Reversi Game</h1>
                <Board />
                <ActionsBar />
                <Loader />
                <AlertBox />
            </div>
        )
    }
}

export default Home;