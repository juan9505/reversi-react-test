import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'actions';

class AlertBox extends Component {

    constructor(props) {
        super(props);
        this.closeAlert = this.closeAlert.bind(this);
    }

    renderAlert() {
        if (this.props.alertState !== "") {
            return (
                <div className="alert-wrapper">
                    <div className="message">
                        <span>{this.props.alertState}</span>
                    </div>
                    <i className="fa fa-times fa-lg" aria-hidden="true" onClick={this.closeAlert}></i>
                </div>
            );
        }
        else {
            return <div></div>;
        }
    }

    closeAlert() {
        this.props.updateBox("");
    }

    render() {
        return (
            this.renderAlert()
        )
    }
}

const mapStateToProps = ({ alertState, gameState }) => {
    return { alertState: alertState, gameState: gameState };
};

export default connect(mapStateToProps, actions)(AlertBox);