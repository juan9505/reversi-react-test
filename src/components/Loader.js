import React, { Component } from 'react';
import { connect } from 'react-redux';

class Loader extends Component {

    constructor(props) {
        super(props);
    }

    renderLoader() {
        if (this.props.loaderState) {
            return (
                <div className="loader-wrapper">
                    <div className="loader"></div>
                </div>
            );
        }
        else {
            return <div></div>;
        }
    }

    render() {
        return (
            this.renderLoader()
        )
    }
}

const mapStateToProps = ({ loaderState }) => {
    return { loaderState: loaderState };
};

export default connect(mapStateToProps)(Loader);