import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'actions';

class BoardCell extends Component {

    constructor(props) {
        super(props);
        this.cellClick = this.cellClick.bind(this);
    }

    cellClick() {
        this.props.setMovement(this.props.position[0], this.props.position[1]);
    }

    render() {
        return (
            <div className="board-col" style={this.props.style} onClick={this.cellClick}>
            </div>
        )
    }
}

export default connect(null, actions)(BoardCell);