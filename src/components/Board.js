import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'actions';
import BoardCell from './BoardCell'

class Board extends Component {

    componentDidMount() {
        this.props.setLoaderState();
        this.props.getState();
    }

    componentWillReceiveProps(nextProps) {
        const whiteCount = nextProps.gameState.whiteCount;
        const blackCount = nextProps.gameState.blackCount;
        if (nextProps.gameState.gameState === "FINALIZED") {
            if (whiteCount > blackCount) {
                nextProps.updateBox("El ganador es el color Blanco");
            }
            else if (whiteCount == blackCount) {
                nextProps.updateBox("¡Es un empate!");
            }
            else {
                nextProps.updateBox("El ganador es el color Negro");
            }
        }
    }

    render() {
        const boardRows = this.props.gameState.boardRows.map((row, j) => {
            const boardCells = row.map((cell, k) => {
                const bgColor = () => {
                    if (cell === 'B') {
                        return { backgroundColor: 'black' }
                    }
                    else if (cell === 'W') {
                        return { backgroundColor: 'white' }
                    }
                    else {
                        return { backgroundColor: 'transparent', border: '1px solid black' }
                    }
                }
                return (
                    <BoardCell key={j + "," + k} position={[j, k]} style={bgColor()}>{cell}</BoardCell>
                );
            });
            return (
                <div key={j} className="board-row row justify-content-center">
                    {boardCells}
                </div>
            );
        });

        return (
            <div className="board-container">
                {boardRows}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return { gameState: state.gameState };
};

export default connect(mapStateToProps, actions)(Board);