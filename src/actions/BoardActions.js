import {
    GAME_STATE_UPDATE,
    GAME_RESET,
    LOADER_UPDATE,
    ALERT_UPDATE
} from './types';
import axios from 'axios';

export const getState = () => {
    return (dispatch) => {
        axios.get('http://35.163.129.163:9000/reversi/game', {
                params: {
                    token: '88152021-e0de-4fe0-ad7b-5fd960a04db0'
                }
            })
            .then(function (response) {
                dispatch({
                    type: GAME_STATE_UPDATE,
                    payload: response.data
                });
                dispatch({
                    type: LOADER_UPDATE,
                    payload: false
                });
            })
            .catch(function (error) {
                dispatch({
                    type: LOADER_UPDATE,
                    payload: false
                });
                if (error.response.status === 401) {
                    dispatch(updateBox("Token inválido"));
                }
            });
    }
};

export const setMovement = (selectY, selectX) => {
    return (dispatch) => {
        dispatch({
            type: LOADER_UPDATE,
            payload: true
        });

        axios({
                method: 'post',
                url: 'http://35.163.129.163:9000/reversi/game/movements',
                params: {
                    token: '88152021-e0de-4fe0-ad7b-5fd960a04db0',
                    x: selectX,
                    y: selectY
                }
            })
            .then(function (response) {
                dispatch(getState());
            })
            .catch(function (error) {
                dispatch({
                    type: LOADER_UPDATE,
                    payload: false
                });
                if (error.response.status === 400) {
                    dispatch(updateBox("El movimiento es inválido"));
                }
                if (error.response.status === 409) {
                    dispatch(updateBox("La posición seleccionada no está vacía"));
                }
                if (error.response.status === 401) {
                    dispatch(updateBox("Token inválido"));
                }
            });
    }
}

export const resetGame = () => {
    return (dispatch) => {
        dispatch({
            type: LOADER_UPDATE,
            payload: true
        });

        axios({
                method: 'delete',
                url: 'http://35.163.129.163:9000/reversi/game',
                params: {
                    token: '88152021-e0de-4fe0-ad7b-5fd960a04db0'
                }
            })
            .then(function (response) {
                dispatch({
                    type: GAME_RESET
                });
                dispatch(getState());
            })
            .catch(function (error) {
                dispatch({
                    type: LOADER_UPDATE,
                    payload: false
                });
                if (error.response.status === 401) {
                    dispatch(updateBox("Token inválido"));
                }
            });
    }
}

export const setLoaderState = () => {
    return (dispatch) => {
        dispatch({
            type: LOADER_UPDATE,
            payload: true
        });
    }
}

export const updateBox = (message) => {
    return (dispatch) => {
        dispatch({
            type: ALERT_UPDATE,
            payload: message
        });
    }
}