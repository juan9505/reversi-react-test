export const GAME_STATE_UPDATE = 'game_state_update';
export const GAME_RESET = 'game_reset';
export const LOADER_UPDATE = 'loader_update';
export const ALERT_UPDATE = 'alert_update';