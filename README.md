# Este proyecto se desarrolló con ReactJS, Redux y usa Webpack para empaquetar la aplicación
* En la carpeta 'src', se encuentra todo el código fuente
* En la carpeta 'public', se encuentran los archivos HTML, CSS y Scripts externos.

## Instrucciones para compilar
> Paso 1: npm install

### Para visualizar en un entorno de pruebas
> Paso 2: npm start.

> Paso 3: Abrir en la dirección: localhost:8080.

### Para visualizar en un entorno de producción:
> Paso 2: npm run postinstall.

> Paso 3: abrir el archivo index.html, que se encuentra en la carpeta 'public'.

#### Si no se desea compilar, simplemente se debe abrir el archivo index.html, en la carpeta 'public', sin realizar los pasos anteriores.



## Realizado por: Juan Felipe Camargo Ramírez